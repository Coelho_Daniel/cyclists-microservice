package bicicletario.microsservico_ciclistas;

import java.util.ArrayList;
import java.util.stream.Collectors;

import POJOs.Cartao;
import POJOs.Ciclista;
import POJOs.Documento;

public class CiclistaDAO {
	private ArrayList<Ciclista> ciclistas = new ArrayList<Ciclista>();
	private static CiclistaDAO ciclistaDao = null; 
	
	CiclistaDAO(){
		this.initializeCiclistasArray();
	}
		
	public static CiclistaDAO instance() {
		System.out.println("Cria a instancia");
		if (ciclistaDao == null) {
			ciclistaDao = new CiclistaDAO();
		} 
		return ciclistaDao;
	}
	
	public static void resetInstance() {
		System.out.println("Reseta a instancia");
		ciclistaDao = null;
	}
	
	private void initializeCiclistasArray() {
		this.ciclistas.add(new Ciclista(
					"01",
					"x@gmail.com",
					"Denis",
					"pimentinha1234",
					true,
					new Documento("CPF", 1234, "foot.png"),
					new Cartao(1234567, "Joao Silva", "01/20", 123)
				));
		
		this.ciclistas.add(new Ciclista(
				"02",
				"y@gmail.com",
				"David",
				"mini123",
				true,
				new Documento("CPF", 7685, "picture.png"),
				new Cartao(879654, "David Cunha", "12/23", 765)
			));
		
		this.ciclistas.add(new Ciclista(
				"03",
				"z@gmail.com",
				"Augusto",
				"Vblind1234",
				true,
				new Documento("CPF", 1234, "foto.png"),
				new Cartao(183628, "Augusto Silva", "07/21", 464)
			));
	}
	
	public int getCiclistasSize() {
		return this.ciclistas.size();
	}
	
	public String fetchIdFromLastCiclista() {
		int positionLastCiclista = this.ciclistas.size() - 1;
		return this.ciclistas.get(positionLastCiclista).getId();
	}
	
	public boolean setUsandoBicicletaById(String id, boolean isUsing) {
		Ciclista c = this.fetchCiclistaById(id);
		
		if(c == null){
			return false;
		}
		
		c.setUsandoBicicleta(isUsing);
		return true;
	}
	
	
	public Ciclista fetchCiclistaById(String id) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			if (this.ciclistas.get(i).getId().equals(id)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public String fetchEmailByCiclistaId(String id) {
		Ciclista c = this.fetchCiclistaById(id);
		return c.getEmail();
	}
	
	public Ciclista fetchCiclistaByName(String nome){
		for(int i=0; i < this.ciclistas.size(); i++) {
			if(this.ciclistas.get(i).getNome().equals(nome)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public String fetchEmailByCiclistaName(String nome) {
		Ciclista c = this.fetchCiclistaByName(nome);
		return c.getEmail();
	}
	
	public Ciclista fetchCiclistaByEmail(String email) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			if (this.ciclistas.get(i).getEmail().equals(email)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}
	
	public String fetchEmailByCiclistaEmail(String email) {
		Ciclista c = this.fetchCiclistaByEmail(email);
		return c.getEmail();
	}
	
	public boolean addCiclista(Ciclista c) {
		c.defineId();
		if (!this.verifyEmail(c.getEmail())) {
			return false;
		}
		this.ciclistas.add(c);
		return true;
	}
	
	private boolean verifyEmail(String email) {
		for (int i=0; i < this.ciclistas.size(); i++) {
			if(this.ciclistas.get(i).getEmail().equals(email)){
				return false;
			}
		}
		return true;
	}
		
	public void deleteCiclista(String id) {
		Ciclista c = this.fetchCiclistaById(id);
		this.ciclistas.remove(c);
	}
	
	public ArrayList<Ciclista> fetchAllCiclistas() {
		return this.ciclistas;
	}
	
	public Cartao fetchCartaoByProprietario(String prop) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			Cartao cartao = this.ciclistas.get(i).getCartao();
			if(cartao.getProprietario().equals(prop)) {
				return cartao;
			}
		}
		return null;
	}
	
	public Cartao fetchCartaoByNumero(int num) {
		for(int i=0; i < this.ciclistas.size(); i++) {
			Cartao cartao = this.ciclistas.get(i).getCartao();
			if(cartao.getNumero() == num) {
				return cartao;
			}
		}
		return null;
	}
	
	
}
