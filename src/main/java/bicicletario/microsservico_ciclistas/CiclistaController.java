package bicicletario.microsservico_ciclistas;

import java.util.ArrayList;

import POJOs.Cartao;
import POJOs.Ciclista;
import POJOs.BicicletaMock;
import POJOs.TrancaMock;
import POJOs.TrancaMock.TrancaStatus;
import POJOs.BicicletaMock.BicicletaStatus;
import io.javalin.http.Context;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class CiclistaController {
	
	
	public static void fetchCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String email = ctx.queryParam("email");
		Ciclista c = null;
		
		if(id != null) {
			c = cDao.fetchCiclistaById(id);
		}else if(nome != null) {
			c = cDao.fetchCiclistaByName(nome);
		}else if(email != null) {
			c = cDao.fetchCiclistaByEmail(email);
		}
		
		if(c == null) {
			ctx.status(500);
		}else{
			ctx.status(200);
			ctx.json(c);
		}
		
	}
	
	public static void fetchAllCiclistas(Context ctx) {
		System.out.println("Chega a entrar");
		CiclistaDAO cDao = CiclistaDAO.instance();
		ArrayList<Ciclista> ciclistas = cDao.fetchAllCiclistas();
		System.out.println(ciclistas);
		if (ciclistas == null) {
			ctx.status(500);
		}else {
			ctx.status(200);
			ctx.json(ciclistas);
		}
		
	}
	
	public static void addCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		Ciclista ciclista = ctx.bodyAsClass(Ciclista.class);
		
		if (ciclista == null) {
			ctx.status(500);
		}else {
			if(cDao.addCiclista(ciclista))
				ctx.status(201);
			else
				ctx.status(500);
		}		
		
	}	
	
	public static void deleteCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		String id = ctx.queryParam("id");
		
		Ciclista c = cDao.fetchCiclistaById(id);
		
		if(id == null || c == null) {
			ctx.status(500);
		}else {
			id = String.valueOf(id);
			cDao.deleteCiclista(id);
			ctx.status(200);
		}	
		
	}
	
	public static void fetchEmailFromCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		Ciclista c = null;
		String email = null;
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String emailParam = ctx.queryParam("email");
		
		if(id != null) {
			email = cDao.fetchEmailByCiclistaId(id);
		}else if(nome != null) {
			email = cDao.fetchEmailByCiclistaName(nome);
		}else if(emailParam != null) {
			email = cDao.fetchEmailByCiclistaEmail(emailParam);
		}
		
		if (email == null) {
			ctx.status(500); 
		}else {
			ctx.status(200); 
			ctx.json(email);
		}
		
	}
	
	public static void fetchCartaoFromCiclista(Context ctx) {
		CiclistaDAO cDao = CiclistaDAO.instance();
		String id = ctx.queryParam("id");
		String proprietario = ctx.queryParam("proprietario");
		String numero = ctx.queryParam("numero");
		Cartao c = null;
		
		if(id != null) {
			c = cDao.fetchCiclistaById(id).getCartao();
		}else if(proprietario != null) {
			c = cDao.fetchCartaoByProprietario(proprietario);
		}else if(numero != null) {
			c = cDao.fetchCartaoByNumero(Integer.parseInt(numero));
		}
		
		if (c == null) {
			ctx.status(500);
		}else {
			ctx.status(200);
			ctx.json(c);
		}
		
	}
	
	public static void alugaBicicleta(Context ctx) {
		String idCiclista = ctx.queryParam("Ciclista");
		String idTotem = ctx.queryParam("Totem");
		String numeroTranca = ctx.queryParam("Tranca");
		String idBicicleta = ctx.queryParam("Bicicleta");
		String idTranca = ctx.queryParam("idTranca");
		String isTest = ctx.queryParam("Teste");
		CiclistaDAO cDao = CiclistaDAO.instance();
		
		
		if(idCiclista == null || idTotem == null || numeroTranca == null
				|| idBicicleta == null || idTranca == null)
		{
			ctx.status(400);
		}
		
		
		if(isTest != null && isTest.toLowerCase().equals("true")) {
			BicicletaMock.criaInstancias();
			TrancaMock.criaInstancias();
			
			BicicletaMock bicicleta = BicicletaMock.getBicicletaById(idBicicleta);
			TrancaMock tranca = TrancaMock.getTrancaById(idTranca);
			
			if(bicicleta == null || tranca == null) {
				ctx.status(400);				
			}
			
			if(bicicleta.getStatus() != BicicletaStatus.DISPONIVEL)
				ctx.status(400);
			
			if(tranca.getStatus() != TrancaStatus.EMUSO)
				ctx.status(400);
			
			bicicleta.setStatus(BicicletaStatus.EMUSO);
			tranca.setStatus(TrancaStatus.DISPONIVEL);
					
		}else {
			HttpResponse abreTrancaResponse = Unirest
					.post("https://javalin-heroku-equipamento.herokuapp.com"
						+ "/liberaTranca"
						+ "?idTotem="+ idTotem 
						+ "&numeroTranca="+ numeroTranca
						+ "&idTranca="+ idTranca
						+ "&idBicicleta"+ idBicicleta).asString();
				
				if(abreTrancaResponse.getStatus() == 400){
					ctx.status(400);
				}
				
				HttpResponse postStatusBicicletaResponse = Unirest
						.post("https://bicicletario.herokuapp.com/statusBicicleta" 
								+ "?emUso=true").asString();
				
				if(postStatusBicicletaResponse.getStatus() == 400){
					ctx.status(400);
				}
		}
		

		Ciclista c = cDao.fetchCiclistaById(idCiclista);
		
		if(c.isUsandoBicicleta())
			ctx.status(400);
		
		c.setUsandoBicicleta(true);
		
		ctx.status(200);
	}
	
	public static void devolveBicicleta(Context ctx){
		String idBicicleta = ctx.queryParam("Bicicleta");
		String idTranca = ctx.queryParam("Tranca");
		String idCiclista = ctx.queryParam("Ciclista");
		String isTest = ctx.queryParam("Teste");
		CiclistaDAO cDao = CiclistaDAO.instance();
		
		if (idBicicleta == null || idTranca == null) {
			ctx.status(400);
		}
		
		if (isTest != null && isTest.toLowerCase().equals("true")){			
			BicicletaMock.criaInstancias();
			TrancaMock.criaInstancias();
			
			BicicletaMock bicicleta = BicicletaMock.getBicicletaById(idBicicleta);
			TrancaMock tranca = TrancaMock.getTrancaById(idTranca);
			
			if(bicicleta == null || tranca == null)
				ctx.status(400);
	
			bicicleta.setStatus(BicicletaStatus.DISPONIVEL);
			tranca.setStatus(TrancaStatus.EMUSO);			
		} else{
			
			HttpResponse postStatusBicicletaResponse = Unirest
					.post("https://bicicletario.herokuapp.com/statusBicicleta" 
							+ "?emUso=false").asString();
			
			HttpResponse abreTrancaResponse = Unirest
					.post("https://javalin-heroku-equipamento.herokuapp.com"
						+ "/statusTranca"
						+ "?ocupada=true").asString();
			
		}
		
		Ciclista c = cDao.fetchCiclistaById(idCiclista);
		
		if(!c.isUsandoBicicleta())
			ctx.status(400);
		
		c.setUsandoBicicleta(false);
		
		ctx.status(200);			
	}


}

