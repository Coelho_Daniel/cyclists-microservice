package POJOs;

public class Documento {
	
	private String tipo;
	private int numero;
	private String foto;
	
	public Documento(){}
	
	public Documento(String tipo, int numero, String foto) {
		this.tipo = tipo;
		this.numero = numero;
		this.foto = foto;
	}

	public String getTipo() {
		return tipo;
	}

	public int getNumero() {
		return numero;
	}

	public String getFoto() {
		return foto;
	}
	

}
