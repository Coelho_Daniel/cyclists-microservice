package POJOs;

import bicicletario.microsservico_ciclistas.CiclistaDAO;

public class Ciclista {
	
	private String id = "";
	private String email = "";
	private String nome = "";
	private String senha = "";
	private boolean indicadorBr = true;
	private boolean usandoBicicleta = false;
	private Documento documento;
	private Cartao cartao;
	
	public Ciclista() {}
	
	public Ciclista(String id, String email, String nome, 
			String senha, boolean indicadorBr, 
			Documento documento, Cartao cartao) {
		this.id = id;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.indicadorBr = indicadorBr;
		this.documento = documento;
		this.cartao = cartao;
	}
	
	public Ciclista(String email, String nome, 
			String senha, boolean indicadorBr, 
			Documento documento, Cartao cartao) {
		this.defineId();
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.indicadorBr = indicadorBr;
		this.documento = documento;
		this.cartao = cartao;
	}
	
	public void defineId() {
		CiclistaDAO cDao = CiclistaDAO.instance(); 
		int lastId = Integer.parseInt(cDao.fetchIdFromLastCiclista());
		String id = null;
		if (lastId + 1 < 10) {
			id = "0" + Integer.toString(lastId + 1);
		}else {
			id = Integer.toString(lastId);
		}
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}

//	public void setEmail(String email) {
//		this.email = email;
//	}

	public String getNome() {
		return nome;
	}

//	public void setNome(String nome) {
//		this.nome = nome;
//	}

	public String getSenha() {
		return senha;
	}

//	public void setSenha(String senha) {
//		this.senha = senha;
//	}

	public boolean isIndicadorBr() {
		return indicadorBr;
	}

//	public void setIndicadorBr(boolean indicadorBr) {
//		this.indicadorBr = indicadorBr;
//	}

	public boolean isUsandoBicicleta() {
		return usandoBicicleta;
	}

	public void setUsandoBicicleta(boolean usandoBicicleta) {
		this.usandoBicicleta = usandoBicicleta;
	}

//	public void setId(String id) {
//		this.id = id;
//	}
	
	public String getId() {
		return this.id;
	}

	public Documento getDocumento() {
		return documento;
	}

//	public void setDocumento(Documento documento) {
//		this.documento = documento;
//	}

	public Cartao getCartao() {
		return cartao;
	}

//	public void setCartao(Cartao cartao) {
//		this.cartao = cartao;
//	}

}
