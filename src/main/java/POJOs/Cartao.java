package POJOs;

public class Cartao {
	
	private int numero;
	private String proprietario;
	private String validade;
	private int codigo;
	
	public Cartao(){}
	
	public Cartao(int numero, String proprietario, String validade, int codigo) {
		this.numero = numero;
		this.proprietario = proprietario;
		this.validade = validade;
		this.codigo = codigo;
	}

	public int getNumero() {
		return numero;
	}

	public String getProprietario() {
		return proprietario;
	}

	public String getValidade() {
		return validade;
	}

	public int getCodigo() {
		return codigo;
	}

	
	

}
