

import io.javalin.Javalin;

import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

import bicicletario.microsservico_ciclistas.CiclistaController;

public class Application {
	
	public static void main(String[] args) {
        Javalin app = Javalin.create(config -> {
        	config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
        }).routes(() -> {
        	path("", () -> {
        		get(ctx -> ctx.result("Hello World"));
        	});
        	path("/ciclista", () -> {
        		get(CiclistaController::fetchCiclista);
        		post(CiclistaController::addCiclista);
        		delete(CiclistaController::deleteCiclista);
        	});
        	path("/ciclistas", () -> {
        		get(CiclistaController::fetchAllCiclistas);
        	});
        	path("/email", () -> {	
        		get(CiclistaController::fetchEmailFromCiclista);
        	});
        	path("/cartaoDeCredito", () -> {
        		get(CiclistaController::fetchCartaoFromCiclista);
        	});
        	path("/alugaBicicleta", () -> {
        		post(CiclistaController::alugaBicicleta);
        	});
        	path("/devolveBicicleta", () -> {
        		post(CiclistaController::devolveBicicleta);
        	});
        }).start(getHerokuAssignedPort());
    }
	
	

	private static int getHerokuAssignedPort() {
	    String herokuPort = System.getenv("PORT");
	    if (herokuPort != null) {
	      return Integer.parseInt(herokuPort);
	    }
	    return 7781;
	  }
	
	private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("User API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("com.bicicletario.bicicletario")
                .path("/bicicletario-swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .defaultDocumentation(doc -> {
//                    doc.json("500", ErrorResponse.class);
//                    doc.json("503", ErrorResponse.class);
                });
        return new OpenApiPlugin(options);
    }
	
}


