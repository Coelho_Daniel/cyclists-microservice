package bicicletario.microsservico_ciclistas;

import static org.junit.Assert.*;

import java.util.ArrayList;

//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import static org.junit.jupiter.api.Assertions.*;

import POJOs.Cartao;
import POJOs.Ciclista;
import POJOs.Documento;

public class CiclistaDaoTest {
	CiclistaDAO cDao;
	
	@Before
	public void init() {
		cDao = CiclistaDAO.instance();
	}
	
	@After
	public void finalize() {
		cDao.resetInstance();
	}
	
	@Test
	public void instanceTest() {		
		assertNotNull(cDao);
	}
	
	@Test
	public void fetchAllCiclistasTest() {
		ArrayList<Ciclista> ciclistas = cDao.fetchAllCiclistas();
		assertTrue(ciclistas.size() > 0);
	}
	
	@Test
	public void addCiclistaTest() {
		Ciclista c = new Ciclista(
				"b@gmail.com",
				"Vilma",
				"Vblind12345",
				true,
				new Documento("CPF", 8936, "foto.png"),
				new Cartao(72846372, "Vilma Duarte", "09/22", 875)
			);
		cDao.addCiclista(c);
		Ciclista ciclistaTest = cDao.fetchCiclistaById("05");
		assertEquals("Vilma", ciclistaTest.getNome());
	}
	
	@Test
	public void getCiclistasSizeTest() {
		int sizeBeforeInsertion = cDao.getCiclistasSize();
		Ciclista c = new Ciclista(
				"b@gmail.com",
				"Vilma",
				"Vblind12345",
				true,
				new Documento("CPF", 8936, "foto.png"),
				new Cartao(72846372, "Vilma Duarte", "09/22", 875)
			);
		cDao.addCiclista(c);
		int sizeAfterInsertion = cDao.getCiclistasSize();
		assertEquals(sizeBeforeInsertion + 1, sizeAfterInsertion);
	}
	
	@Test
	public void fetchCiclistaByIdTest() {
		Ciclista c = cDao.fetchCiclistaById("01");
		assertEquals("Denis", c.getNome());
	}
	
	@Test
	public void fetchCiclistaByNameTest() {
		Ciclista c = cDao.fetchCiclistaByName("Denis");
		assertEquals("Denis", c.getNome());
	}
	
	@Test
	public void fetchCiclistaByEmailTest() {
		Ciclista c = cDao.fetchCiclistaByEmail("z@gmail.com");
		assertEquals("z@gmail.com", c.getEmail());
	}
		
	@Test
	public void deleteCiclistaTest(){
		cDao.deleteCiclista("01");
		Ciclista c = cDao.fetchCiclistaById("01");
		assertNull(c);
	}
	
	
	@Test 
	public void fetchCartaoByProprietarioTest(){
		Cartao cartao = cDao.fetchCartaoByProprietario("Joao Silva");
		assertEquals("Joao Silva", cartao.getProprietario());
	}
	
	@Test
	public void fetchCartaoByNumeroTest() {
		Cartao cartao = cDao.fetchCartaoByNumero(1234567);
		assertEquals("Joao Silva", cartao.getProprietario());
	}
	

}
