package bicicletario.microsservico_ciclistas;
import POJOs.Cartao;
import POJOs.Ciclista;
import POJOs.Documento;

import static org.junit.Assert.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CiclistaControllerTest {
	
	private CiclistaDAO cDao;
	private Context ctx = mock(Context.class);; 
	
	
	@Before
	public void init() {
		cDao = CiclistaDAO.instance();
	}
	
	@After
	public void finalize() {
		CiclistaDAO.resetInstance();
	}
	
	
	
	@Test
	public void fetchAllCiclistasTest() {
		CiclistaController.fetchAllCiclistas(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void fetchCiclistaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		CiclistaController.fetchCiclista(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void deleteCiclistaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		CiclistaController.deleteCiclista(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void fetchEmailFromCiclistaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		CiclistaController.fetchEmailFromCiclista(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void fetchCartaoFromCiclistaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		CiclistaController.fetchCartaoFromCiclista(ctx);
		verify(ctx).status(200);
	}
	
	@Test
	public void alugaBicicletaTest() {
		when(ctx.queryParam("id")).thenReturn("01");
		CiclistaController.alugaBicicleta(ctx);
		verify(ctx).status(200);
	}

}
