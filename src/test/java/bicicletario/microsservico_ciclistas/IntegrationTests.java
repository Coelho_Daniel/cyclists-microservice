package bicicletario.microsservico_ciclistas;

import POJOs.Cartao;
import POJOs.Ciclista;
import POJOs.Documento;

import static org.junit.Assert.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Test;

public class IntegrationTests {

	private CiclistaDAO cDao;
	private Context ctx = mock(Context.class); 
	
	
	@Before
	public void init() {
		cDao = CiclistaDAO.instance();
	}
	
	@After
	public void finalize() {
		CiclistaDAO.resetInstance();
	}
	
	@Test
	public void getCiclistasTest() {
		HttpResponse response = Unirest.get("http://localhost:7781/ciclistas").asString();
		assertThat(response.getStatus()).isEqualTo(200);	
	}

	@Test 
	public void getCiclistaTest() {
		
		String ciclistaJson1 = JavalinJson.toJson(cDao.fetchCiclistaById("02"));
		HttpResponse response1 = Unirest.get("http://localhost:7781/ciclista?id=02").asString();
		assertThat(response1.getBody()).isEqualTo(ciclistaJson1);
		assertThat(response1.getStatus()).isEqualTo(200);
		
		String ciclistaJson2 = JavalinJson.toJson(cDao.fetchCiclistaByName("David"));
		HttpResponse response2 = Unirest.get("http://localhost:7781/ciclista?nome=David").asString();
		assertThat(response2.getBody()).isEqualTo(ciclistaJson2);
		assertThat(response2.getStatus()).isEqualTo(200);
		
		
		String ciclistaJson3 = JavalinJson.toJson(cDao.fetchCiclistaByEmail("z@gmail.com"));
		HttpResponse response3 = Unirest.get("http://localhost:7781/ciclista?email=z@gmail.com").asString();
		assertThat(response3.getBody()).isEqualTo(ciclistaJson3);
		assertThat(response3.getStatus()).isEqualTo(200);
	}
	
	@Test 
	public void getCiclistaTestFail() {
		
		HttpResponse response1Error = Unirest.get("http://localhost:7781/ciclista?id=-1000").asString();
		assertThat(response1Error.getStatus()).isEqualTo(500);
		
		HttpResponse response2Error = Unirest.get("http://localhost:7781/ciclista?nome=wdbhdbhdbh").asString();
		assertThat(response2Error.getStatus()).isEqualTo(500);
		
		HttpResponse response3Error = Unirest.get("http://localhost:7781/ciclista?email=ndwdjdwjnd").asString();
		assertThat(response3Error.getStatus()).isEqualTo(500);
	}
	
	@Test
	public void postCiclistaTest() {
		
		Ciclista c = new Ciclista(
				"cesar@gmail.com",
				"Cesar",
				"nsidnsdi",
				true,
				new Documento("CPF", 73986, "foto.png"),
				new Cartao(234244, "Cesar", "01/20", 534)
			);
		
		String ciclistaJson = JavalinJson.toJson(c);
		HttpResponse response = Unirest.post("http://localhost:7781/ciclista").body(ciclistaJson).asString();
		assertThat(response.getStatus()).isEqualTo(201);
	}
	
	@Test
	public void postCiclistaTestFail() {		
		Ciclista c = null;
		HttpResponse response = Unirest.post("http://localhost:7781/ciclista").body(c).asString();
		assertThat(response.getStatus()).isEqualTo(400);
	}
	
	@Test
	public void deleteCiclistaTest() {
		HttpResponse response1 = Unirest.delete("http://localhost:7781/ciclista?id=01").asString();
		assertThat(response1.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void deleteCiclistaTestFail() {
		HttpResponse response = Unirest.delete("http://localhost:7781/ciclista?id=-7656").asString();
		assertThat(response.getStatus()).isEqualTo(500);
	}

	@Test
	public void getEmailTest() {
		String emailJson1 = JavalinJson.toJson(cDao.fetchCiclistaById("02").getEmail());
		HttpResponse response1 = Unirest.get("http://localhost:7781/email?id=02").asString();
		assertThat(response1.getBody()).isEqualTo(emailJson1);
		
		String emailJson2 = JavalinJson.toJson(cDao.fetchCiclistaByName("David").getEmail());
		HttpResponse response2 = Unirest.get("http://localhost:7781/email?nome=David").asString();
		assertThat(response2.getBody()).isEqualTo(emailJson2);
		
		String emailJson3 = JavalinJson.toJson(cDao.fetchCiclistaByEmail("z@gmail.com").getEmail());
		HttpResponse response3 = Unirest.get("http://localhost:7781/email?email=z@gmail.com").asString();
		assertThat(response3.getBody()).isEqualTo(emailJson3);
	}
	
	@Test
	public void getEmailTestFail() {
		HttpResponse response1 = Unirest.get("http://localhost:7781/email?id=-102992").asString();
		assertThat(response1.getStatus()).isEqualTo(500);
		
		HttpResponse response2 = Unirest.get("http://localhost:7781/email?nome=dsdsds").asString();
		assertThat(response2.getStatus()).isEqualTo(500);
		
		HttpResponse response3 = Unirest.get("http://localhost:7781/email?email=sdfedwd").asString();
		assertThat(response3.getStatus()).isEqualTo(500);
	}

	@Test 
	public void getCartaoDeCreditoTest() {
		String cartaoJson1 = JavalinJson.toJson(cDao.fetchCiclistaById("02").getCartao());
		HttpResponse response1 = Unirest.get("http://localhost:7781/cartaoDeCredito?id=02").asString();
		assertThat(response1.getBody()).isEqualTo(cartaoJson1);
		
		String cartaoJson2 = JavalinJson.toJson(cDao.fetchCartaoByProprietario("David Cunha"));
		HttpResponse response2 = Unirest.get("http://localhost:7781/cartaoDeCredito?proprietario=David Cunha").asString();
		assertThat(response2.getBody()).isEqualTo(cartaoJson2);
		
		String cartaoJson3 = JavalinJson.toJson(cDao.fetchCartaoByNumero(183628));
		HttpResponse response3 = Unirest.get("http://localhost:7781/cartaoDeCredito?numero=183628").asString();
		assertThat(response3.getBody()).isEqualTo(cartaoJson3);
	}
	
	@Test 
	public void getCartaoDeCreditoTestFail() {
		HttpResponse response1 = Unirest.get("http://localhost:7781/cartaoDeCredito?id=-1828").asString();
		assertThat(response1.getStatus()).isEqualTo(500);
		
		HttpResponse response2 = Unirest.get("http://localhost:7781/cartaoDeCredito?proprietario=dssfa").asString();
		assertThat(response2.getStatus()).isEqualTo(500);
		
		HttpResponse response3 = Unirest.get("http://localhost:7781/cartaoDeCredito?numero=-027182").asString();
		assertThat(response3.getStatus()).isEqualTo(500);
	}
	
	@Test 
	public void alugaBicicletaTest() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=01"
						+ "&Totem=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=01"
						+ "&idTranca=01"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test 
	public void alugaBicicletaTestFail() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=01"
						+ "&Totem=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=10"
						+ "&idTranca=01"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}
	
	@Test 
	public void devolveBicicletaTest() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=02"
						+ "&Tranca=02"
						+ "&Bicicleta=02"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test 
	public void devolveBicicletaTestFail() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "idCiclista=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=-1000"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}

}
